#!/usr/anaconda/env python
# -*- coding: utf-8 -*-
# author: uestcwdh
# @Time: 2020/1/30 12:22
import json
mapping = {
    'name': 'Wan Dehua',
    'age': 23,
    'gender': 'male',
    'tel': '13197911586',
}
# generate a nicely indented string representation
# also normalizes the order of the dictionary keys for better legibility.
print(json.dumps(mapping, indent=4, sort_keys=True))
mapping['school'] = {'南昌二中', '南京邮电大学', '电子科技大学'}
# # json.jumps() support only primitive data type
# print(json.dumps(mapping))         # cause TypeError

# use pprint to solve the above problem
import pprint
pprint.pprint(mapping, indent=4)
