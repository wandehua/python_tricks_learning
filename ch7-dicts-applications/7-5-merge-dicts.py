#!/usr/anaconda/env python
# -*- coding: utf-8 -*-
# author: uestcwdh
# @Time: 2020/1/30 12:05

xs = {'a': 1, 'b': 2}
ys = {'b': 3, 'c': 4}
# solution 1: update() -- iterate over all of items, add new and overwrite the same
# final update() determine the key-value conflicts
zs = {}
zs.update(xs)
zs.update(ys)
print(zs)   # res: {'a': 1, 'b': 3, 'c': 4}

# solution2 :use dict() built-in combined with unpacking ops: **
# conflict rules are just the same to solution 1
ws = dict(xs, **ys)
print(ws)   # res: {'a': 1, 'b': 3, 'c': 4}
# another form
ws = {**xs, **ys}
print(ws)
