#!/usr/anaconda/env python
# -*- coding: utf-8 -*-
# author: uestcwdh
# @Time: 2020/1/30 11:46


# use dicts to achieve switch()
# and by using its get() method ,we can get default results
def achieve_switch(ops, x, y):
    return{
        'add': lambda: x + y,
        'minus': lambda: x - y,
        'mul': lambda: x * y,
        'div': lambda: x / y,
    }.get(ops, lambda: None)()


if __name__ == '__main__':
    print(achieve_switch('mul', 3, 5))
    print(achieve_switch('mod', 3, 5))