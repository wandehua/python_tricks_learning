"""
use '*' to unpack tuples, lists and generators into positional arguments
use '**' to unpack the dictionary
"""


def print_vec(x, y, z):
    print('<%s, %s, %s>' % (x, y, z))


if __name__ == '__main__':
    tp = (x for x in range(3))
    lst = [x for x in range(3)]
    dic = {'y': 1, 'x': 0, 'z': 2}
    print_vec(*tp)
    print_vec(*lst)
    print_vec(**dic)
    # especially
    print_vec(*dic)
