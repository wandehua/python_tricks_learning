# anonymous func--> lambda -->single expression functions.
res = (lambda x, y: x + y)(3, 5)
print(res)
lst = sorted(range(-5, 6), key=lambda x: x ** 2, reverse=True)
print(lst)


def create_adder(n):
    return lambda x: x + n


# 返回函数，self.n = 3, add(x)
plus_3 = create_adder(3)
print(plus_3(4))
