#!/usr/anaconda/env python
# -*- coding: utf-8 -*-
# author: uestcwdh
# @Time: 2020/1/26 10:13
"""
1.record data structures provide a fixed number of fields,
where each field can have a name and may also have a different type.
"""
# dict -- simple data objects
from typing import NamedTuple
from collections import namedtuple
import dis
student1 = {
    'name': 'Wan Dehua',
    'sid': '201922080925',
    'age': 22,
    'gender': 'male',
    'isMaster': True,
}

print(student1)
# dict are mutable
student1['age'] = 23
student1['Married'] = False
print(student1)

# tuple -- immutable groups of objects
# executive steps of tuple anf list
dis.dis(compile("(23,'a','b','c')", '', 'eval'))
print()
dis.dis(compile("[23,'a','b','c']", '', 'eval'))
# tuple e.g.
# You can’t give names to individual properties stored in a tuple.
# fields: color, price, automatic
car1 = ('red', 120000, False)
car2 = ('blue', 240000, True)
print('car1= ', car1)
print('car2= ', car2)
# Tuples are immutable:
# car2[1] = 12
# TypeError: "'tuple' object does not support item assignment"
# No protection against missing/extra fields
# or a wrong order:
car3 = (349545, 'yellow', True, 'green')
print(car3)

# nametuples -- clean up code and make it more readable
Car = namedtuple('Car', 'color price automatic')
car1 = Car('red', 146758, True)
print(car1)
print('price:', car1.price)
# # fields are immutable
# car1.price = 360000
# print('modified price: ', car1.price)
# # AttributeError: can't set attribute


# typing -- improved namedtuples
# the main difference being an updated syntax for defining new record types
# and added support for type hints.


# fields are immutable
class Train(NamedTuple):
    color: str
    mileage: float
    automatic: bool


train1 = Train('green', 190384.9, True)
print(train1)
