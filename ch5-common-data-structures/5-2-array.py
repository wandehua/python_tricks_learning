from array import array

arr = array('f', [1.0, 1.1, 1.2, 1.3, 1.4])
arr.append(1.5)
print(arr)
del arr[-1]
print(arr)
# # TypeError: must be real number, not str
# arr[0] = 'hello'

# str –-> Immutable Arrays of Unicode Characters
arr = 'abcdef'
print(arr[-1])
# arr[-1] = 'e'
# del arr[-1]
print(list(arr))
str1 = ''.join(list(arr))
print(str1)

# The bytearrays type is a mutable sequence of integers in the range 0 <= x <= 255
# bytearrays can be modified freely
arr = bytearray((0, 1, 2, 3, 4, 5))
print(arr)
arr[0] = 23
print(arr)
arr.append(20)
print(arr)
del arr[-1]
print(arr)
# arr[-1] = 'hello'
# arr[-1] = 300
print(bytes(arr))