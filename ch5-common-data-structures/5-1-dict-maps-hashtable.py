"""
1. OrderDict --> if key order is important for your algorithm to work, it’s best to
communicate this clearly by explicitly using the OrderDict class.
2. defaultdict --> Return Default Values for Missing Keys
3. ChainMap --> groups multiple dictionaries into a single mapping
4. MappingProxyType –-> A Wrapper for Making Read-Only Dictionaries
"""
from collections import OrderedDict, defaultdict, ChainMap
from types import MappingProxyType
squares = {x: x * x for x in range(6)}
print(squares)
# OrderedDict
d = OrderedDict(one=1, two=2, three=3)
d['four'] = 4
print(d)
print(d.keys())
# defaultdict
dd = defaultdict(list)
dd['dogs'].append('Sam')
dd['dogs'].append('BaGong')
print(dd['dogs'])
# ChainMap --> searches each collection in the chain from left to right
dict1 = {'one': 1, 'two': 2}
dict2 = {'three': 3, 'four': 4}
chain = ChainMap(dict1, dict2)
print(chain)
print(chain['three'])
# print(chain['missing'])
writable = {'one': 1, 'two': 2}
# MappingProxyType
read_only = MappingProxyType(writable)
print(read_only['one'])
writable['one'] = 11
print(read_only)
# read_only['two'] = 2