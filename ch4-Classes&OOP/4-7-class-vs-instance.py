class Panda:
    num_legs = 4

    def __init__(self, name):
        self.name = name


class CountObjects:
    num_instance = 0

    def __init__(self):
        self.__class__.num_instance += 1


class BadCountObjects:
    num_instance = 0

    def __init__(self):
        self.num_instance += 1


if __name__ == '__main__':
    tuantuan = Panda('tuantuan')
    yuanyuan = Panda('yuanyuan')
    print(tuantuan.num_legs, yuanyuan.num_legs)
    # modifying a class variable
    Panda.num_legs = 6
    print(tuantuan.num_legs, yuanyuan.num_legs)
    # modifying a instance variable
    tuantuan.num_legs = 8
    print(tuantuan.num_legs, yuanyuan.num_legs)

    # test CountObjects
    # did not initialize without '()'
    print(CountObjects.num_instance)
    # initialized
    print(CountObjects().num_instance)
    print(CountObjects().num_instance)
    print(CountObjects().num_instance)
    print(CountObjects().num_instance)

    # test BadCountObjects
    # did not initialize without '()'
    print(BadCountObjects.num_instance)
    # initialized
    print(BadCountObjects().num_instance)
    print(BadCountObjects().num_instance)
    print(BadCountObjects().num_instance)
    print(BadCountObjects().num_instance)





