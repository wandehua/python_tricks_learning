"""
1.“write once, read many” principle.
2. Each object stored in them can be accessed through a unique
(human-readable) identifier.
3. Namedtuples can be an easy way to clean up your code and to
 make it more readable by enforcing a better structure for your data
"""
from collections import namedtuple
import json

# define a namedtuple
athlete = namedtuple('athlete', ['height', 'weight'])
# instantiate
one_athlete = athlete(1.83, 73)
# ways to access fields --> use '.' or '[index]
# method 1-2
print(str(one_athlete.height) + '\n' + str(one_athlete[1]))
# method 3
h, w = one_athlete
print(str(h) + '\n' + str(w))
# method 4
print(*one_athlete)
other_athlete = athlete(*one_athlete)
print(other_athlete)


# subclassing namedtuple
class NBAPlayer(athlete):
    def inche_height(self):
        return float(self.height) / 0.3048


if __name__ == '__main__':
    James = NBAPlayer(2.06, 113.4)
    print('{} inches'.format(James.inche_height()))
    # use the base tuple’s _fields property to create hierarchies of namedtuples:
    JumpPlayer = namedtuple('JumpPlayer', athlete._fields + ('jump_height',))
    Isynbayewa = JumpPlayer(1.72, 45, 5.06)
    print(type(Isynbayewa))
    print(Isynbayewa)

    # _asdict() --> helper method 1
    print(James._asdict())
    # dict2str
    print(json.dumps(James._asdict()))

    # _replace() --> helper method 2
    James = James._replace(weight=114)
    print(James)
    # _make --> helper method 3
    Curry = NBAPlayer._make([1.92, 85.0])
    print(Curry)