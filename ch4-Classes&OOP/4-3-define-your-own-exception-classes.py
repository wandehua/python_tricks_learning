"""
1.defining your own error types can be of great value
2.make potential error cases stand out clearly
3.functions and modules will become more maintainable
4.use custom error types to provide additional debugging information
"""


# e.g.
class BaseValidationError(ValueError):
    pass


class NameTooShortError(BaseValidationError):
    pass


class NameTooLongError(BaseValidationError):
    pass


def validate_short_input(name):
    if len(name) < 5:
        raise NameTooShortError(name)


def validate_long_input(name):
    if len(name) > 20:
        raise NameTooLongError(name)


def report_validate_err(error):
    print(err.__class__.__name__ + ': %s' % name)


if __name__ == '__main__':

    name = input("please input your name:\n")
    try:
        validate_short_input(name)
        validate_long_input(name)
    except BaseValidationError as err:
        report_validate_err(err)