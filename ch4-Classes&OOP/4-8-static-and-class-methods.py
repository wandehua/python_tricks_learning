class GraduateStudent:
    def behavior(self):
        return 'instance behavior performed', self

    @classmethod
    def class_behavior(cls):
        return 'class behavior performed', cls

    @staticmethod
    def static_behavior():
        return 'static behavior performed'


class Hamburger:
    default_thick_level = 2

    def __init__(self, ingredients, thick_level):
        self.ingredients = ingredients
        self.thick_level = thick_level

    def __repr__(self):
        return (f'Hamburger({self.ingredients!r},'
                f'{self.thick_level!r})')

    def size(self):
        return self.compute_weight(self.thick_level)

    @staticmethod
    def compute_weight(n):
        return 100 * n + 300

    @classmethod
    def new_orleans(cls):
        return cls(['bread', 'chicken', 'lettuce'], cls.default_thick_level)

    @classmethod
    def spicy_beef(cls):
        return cls(['bread', 'beef', 'lettuce', 'pepper jam'], cls.default_thick_level)


if __name__ == '__main__':
    obj = GraduateStudent()
    print(obj.behavior())
    print(obj.class_behavior())
    print(obj.static_behavior())
    # hamburger test
    ingre = ['bread', 'vegetables', 'meat']
    test = Hamburger(ingre, 4)
    print(repr(test))
    print(test.size())
    print(Hamburger.compute_weight(4))
    print(Hamburger.new_orleans())
    print(Hamburger.spicy_beef())
