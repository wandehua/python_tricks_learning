import datetime
"""
The result of __str__ should be readable. 
The result of __repr__ should be unambiguous.
Always add a __repr__ to your classes.
"""


class Fruit:
    def __init__(self, name, color, price_per_kg):
        self.name = name
        self.color = color
        self.price_per_kg = price_per_kg

    # recommended strongly
    # using !r to make sure the output string uses repr()
    def __repr__(self):
        return (f'{self.name!r}('
                f'{self.color!r}, {self.price_per_kg!r})')

    # optional
    def __str__(self):
        return f'{self.name} is {self.color}, and it is sold {self.price_per_kg} per kg'


if __name__ == '__main__':
    test = Fruit('apple', 'red', 8.99)
    # description of certain fruit by method --> '__str__'
    print(test)
    print(repr(test))
    # analyse the differences between __str__ and __repr__
    today = datetime.date.today()
    print(today)
    print(repr(today))
