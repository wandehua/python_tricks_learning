from string import Template
# use 4 different ways to output 'Hey Bob, there is a 0xbadc0ffee error!'
error = 50159747054
name = 'Bob'
# method 1
print("Hey %s, there is a 0x%x error!" % (name, error))
# or use map to simplify it
print("Hey %(name)s, there is a 0x%(error)x error!" % {
    "name": name, "error": error
})
# method 2
print("Hey {}, there is a {} error!".format(name, hex(error)))
# method 3
print(f"Hey {name}, there is a {error:#x} error!")
# method 4
t = Template("Hey $name, there is a $error error!")
rel = t.substitute(name=name, error=hex(error))
print(rel)
