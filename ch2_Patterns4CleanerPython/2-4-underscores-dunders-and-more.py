"""
Single underscores: a Python naming convention
• Single Leading Underscore: _var (only hint that it intended for internal use)
• Single Trailing Underscore: var_ (is used by convention to avoid naming conflicts with Python keywords)
• Double Leading Underscore: __var (rewrite the attribute name in order to avoid naming conflicts in subclasses)
• Double Leading and Trailing Underscore: __var__ 
• Single Underscore: _ (temporary variable)
"""


class Test:
    def __init__(self):
        self.foo = 11
        self._bar = 23
        self.__baz = 23

    @property
    def bar(self):
        return self._bar


class ExtendedTest(Test):
    def __init__(self):
        super().__init__()
        self.foo = 'overridden'
        self._bar = 'overridden'
        self.__baz = 'overridden'

    @property
    def bar(self):
        return self._bar


if __name__ == '__main__':
    t = Test()
    print(dir(t))
    print(t.bar)
    et = ExtendedTest()
    print(dir(et))
    print(et.bar)
    # use of '_'(case 1)
    car = ('red', 'auto', 12, 3812.4)
    color, _, _, mileage = car
    print(color, mileage)
    # use of '_'(case 2)
    for _ in range(10):
        print('hello, goodbye&hello')

