# 使用声明的旨在告知不可恢复的错误
def compute_course_score(stu, final_ratio):
    score = int(stu['final_grade'] * final_ratio + stu['usual_grade'] * (1 - final_ratio))
    # 声明返回结果的范围，告诉我们程序所发生的不可能的情况
    assert 0 <= score <= 100, '成绩异常'
    '''
    above assertion will be transformed into following forms:
    if __debug__: 
        if not 0 <= score <= 100: 
            raise AssertionError(expression2)
    '''
    return score


if __name__ == '__main__':
    # student = {'name': '万德华', 'final_grade': 75, 'usual_grade': 90}
    # 175 * 0.8 = 140 > 100 --> 不可能发生的情况
    student = {'name': '万德华', 'final_grade': 175, 'usual_grade': 90}
    print(compute_course_score(student, 0.8))